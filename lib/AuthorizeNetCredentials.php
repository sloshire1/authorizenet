<?php

class AuthorizeNetCredentials {

    private static function getBoolConstant(string $name, bool $defaultValue = false): bool {
        if (defined($name)) {
            return constant($name);
        }

        if (function_exists("env")) {
            return env($name, $defaultValue);
        }

        return $defaultValue;
    }

    private static function getStringConstant(string $name, string $defaultValue = ""): string {
        if (defined($name)) {
            return constant($name);
        }

        if (function_exists("env")) {
            return env($name, $defaultValue);
        }

        return $defaultValue;
    }

    /**
     * Get the value of the authorizenet api login id from a predefined constant or the environment file if available.
     * @param string $defaultValue The default value to use when the requested value is not available.
     * @return string
     */
    public static function apiLoginId(string $defaultValue = ""): string {
        return self::getStringConstant("AUTHORIZENET_API_LOGIN_ID", $defaultValue);
    }

    /**
     * Get the authorizenet transaction key from a predefined constant or the environment file if available.
     * @param string $defaultValue The default value to use when the requested value is not available.
     * @return string
     */
    public static function transactionKey(string $defaultValue = ""): string {
        return self::getStringConstant("AUTHORIZENET_TRANSACTION_KEY", $defaultValue);
    }

    /**
     * Get the sandbox flag from a predefined constant or the environment file if available.
     * @param bool $defaultValue The default value to use when the requested value is not available.
     * @return bool
     */
    public static function isSandbox(bool $defaultValue = false): bool {
        return self::getBoolConstant("AUTHORIZENET_SANDBOX", $defaultValue);
    }

    /**
     * Get the log file name from a predefined constant or the environment file if available.
     * @param string $defaultValue The default value to use when the requested value is not available.
     * @return string
     */
    public static function logfile(string $defaultValue = "authorize-net.log"): string {
        return self::getStringConstant("AUTHORIZENET_LOG_FILE", $defaultValue);
    }

    /**
     * Get the md5 setting from a predefined constant or the environment file if available.
     * @param string $defaultValue The default value to use when the requested value is not available.
     * @return string
     */
    public static function md5Setting(string $defaultValue = ""): string {
        return self::getStringConstant("AUTHORIZENET_MD5_SETTING", $defaultValue);
    }
}
